<?php
/**
 * @file
 * cod_session.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function cod_session_taxonomy_default_vocabularies() {
  return array(
    'audience' => array(
      'name' => 'Target Audience',
      'machine_name' => 'audience',
      'description' => 'Who is this session aimed at?',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'session_category' => array(
      'name' => 'Session Strand',
      'machine_name' => 'session_category',
      'description' => 'The session category, selected by the presenter.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-8',
    ),
    'session_format' => array(
      'name' => 'Session Format',
      'machine_name' => 'session_format',
      'description' => 'The session format, selected by the presenter.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '-9',
    ),
    'session_keywords' => array(
      'name' => 'Session Keywords',
      'machine_name' => 'session_keywords',
      'description' => 'Key words entered by the presenter.',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
