<?php
/**
 * @file
 * cod_community.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cod_community_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_admin|user|user|form';
  $field_group->group_name = 'group_profile_admin';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Administrative',
    'weight' => '0',
    'children' => array(
      0 => 'field_profile_admin_comments',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Administrative',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_profile_admin|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_interests|user|user|form';
  $field_group->group_name = 'group_profile_interests';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_profile_public';
  $field_group->data = array(
    'label' => 'Interests',
    'weight' => '19',
    'children' => array(
      0 => 'field_session_category',
      1 => 'field_session_audience',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Interests',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'Let us know which topics areas interest you, so we can help suggest sessions you might want to see.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_profile_interests|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_private|user|user|form';
  $field_group->group_name = 'group_profile_private';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Private profile information',
    'weight' => '8',
    'children' => array(
      0 => 'field_profile_accessibility',
      1 => 'field_profile_location',
      2 => 'field_profile_phone',
      3 => 'field_profile_dietary',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Private profile information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'This private information is visible only to you and to conference organisers.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_profile_private|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_public|user|user|form';
  $field_group->group_name = 'group_profile_public';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Public profile information',
    'weight' => '4',
    'children' => array(
      0 => 'field_profile_first',
      1 => 'field_profile_job_title',
      2 => 'field_profile_last',
      3 => 'field_profile_org',
      4 => 'field_attendee_list',
      5 => 'field_profile_facebook',
      6 => 'field_profile_googleplus',
      7 => 'field_profile_linkedin',
      8 => 'field_profile_twitter',
      9 => 'group_profile_interests',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Public profile information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => 'This public profile information is visible to everyone.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_profile_public|user|user|form'] = $field_group;

  return $export;
}
