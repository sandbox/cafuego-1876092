<?php
/**
 * @file
 * cod_community.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cod_community_user_default_permissions() {
  $permissions = array();

  // Exported permission: cancel account.
  $permissions['cancel account'] = array(
    'name' => 'cancel account',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: create field_profile_admin_comments.
  $permissions['create field_profile_admin_comments'] = array(
    'name' => 'create field_profile_admin_comments',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_profile_admin_comments.
  $permissions['edit field_profile_admin_comments'] = array(
    'name' => 'edit field_profile_admin_comments',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_profile_admin_comments.
  $permissions['edit own field_profile_admin_comments'] = array(
    'name' => 'edit own field_profile_admin_comments',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_profile_admin_comments.
  $permissions['view field_profile_admin_comments'] = array(
    'name' => 'view field_profile_admin_comments',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_profile_admin_comments.
  $permissions['view own field_profile_admin_comments'] = array(
    'name' => 'view own field_profile_admin_comments',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
