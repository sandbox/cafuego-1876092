<?php
/**
 * @file
 */

/**
 * Implements hook_field_formatter_info().
 */
function cod_community_field_formatter_info() {
  return array(
    'cod_community_twitter_link' => array(
      'label' => t('Twitter, @handle as link'),
      'field types' => array('text'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function cod_community_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'cod_community_twitter_link':
      // Common case: each value is displayed individually in a sub-element
      // keyed by delta. The field.tpl.php template specifies the markup
      // wrapping each value.
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'cod_community_twitter_handle',
          '#item' => $item,
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_theme().
 */
function cod_community_theme($existing, $type, $theme, $path) {
  return array(
    'cod_community_twitter_handle' => array(
      'variables' => array(
        'item' => NULL,
      ),
    ),
  );
}

/**
 * Theme function; Twitter handle link.
 */
function theme_cod_community_twitter_handle($variables) {
  $item = $variables['item'];

  // Early return.
  if (empty($item['value'])) {
    return;
  }

  // If a link, just return the link.
  if (strpos($item['value'], 'http') === 0) {
    $parts = parse_url($item['value']);
    $twitter = basename($parts['path']);
    $link = $parts['$path'];
  }
  else {
    // Remove the @ if present.
    $twitter = strtr($item['value'], array('@' => ''));
    $link = t('http://twitter.com/@twitter', array('@twitter' => $twitter));
  }

  // Return a pretty link and an @handle.
  return l(t('@@twitter', array('@twitter' => $twitter)), $link, array('attributes' => array('rel' => 'nofollow')));
}
