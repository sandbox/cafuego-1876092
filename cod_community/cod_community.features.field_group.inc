<?php
/**
 * @file
 * cod_community.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cod_community_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_admin|user|user|default';
  $field_group->group_name = 'group_profile_admin';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Administrative',
    'weight' => '10',
    'children' => array(
      0 => 'field_profile_admin_comments',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => 'Administrative',
      'formatter' => '',
    ),
  );
  $export['group_profile_admin|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_admin|user|user|form';
  $field_group->group_name = 'group_profile_admin';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Administrative',
    'weight' => '0',
    'children' => array(
      0 => 'field_profile_admin_comments',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Administrative',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => 'Administrative fields are not visible to users, only to administrators.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_profile_admin|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_private|user|user|default';
  $field_group->group_name = 'group_profile_private';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Private Information',
    'weight' => '12',
    'children' => array(
      0 => 'field_attendee_list',
      1 => 'field_profile_accessibility',
      2 => 'field_profile_location',
      3 => 'field_profile_phone',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(),
    ),
  );
  $export['group_profile_private|user|user|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_private|user|user|form';
  $field_group->group_name = 'group_profile_private';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Private Information',
    'weight' => '4',
    'children' => array(
      0 => 'field_profile_accessibility',
      1 => 'field_attendee_list',
      2 => 'field_profile_phone',
      3 => 'field_profile_location',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Private Information',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => 'This information is only visible to you and site administrators.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_profile_private|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_profile_public|user|user|form';
  $field_group->group_name = 'group_profile_public';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Public Information',
    'weight' => '3',
    'children' => array(
      0 => 'field_profile_first',
      1 => 'field_profile_interests',
      2 => 'field_profile_job_title',
      3 => 'field_profile_last',
      4 => 'field_profile_org',
      5 => 'field_profile_facebook',
      6 => 'field_profile_googleplus',
      7 => 'field_profile_linkedin',
      8 => 'field_profile_twitter',
      9 => 'picture',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Public Information',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => 'This information is visible in your public profile.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_profile_public|user|user|form'] = $field_group;

  return $export;
}
